# Google Sheets Helper Libary

A library to easily add data to CMSC 436 formatted Sheets, and an example app to demonstrate usage.

## Installation

Download the latest `googlesheetshelper-VERSION-release.aar` from the [Downloads](https://bitbucket.org/0queue/google-sheets-helper-library/downloads/) section.  Then follow the instructions [here](https://developer.android.com/studio/projects/android-library.html#AddDependency) to import the library.  Instead of doing steps 2 and 3 it is also possible to add the dependency graphically, by right clicking on your app module, going to `Open Module Settings`, then the `Dependencies` tab, clicking the green plus on the right and adding a dependency on the new `.aar` module.  Lastly, since `.aar`s don't have any dependency information included, add these lines to the dependencies section of your app's `build.gradle`:

```
compile 'com.google.android.gms:play-services-auth:10.2.1'
compile('com.google.api-client:google-api-client-android:1.22.0') {
    exclude group: 'org.apache.httpcomponents'
    exclude group: 'com.google.code.findbugs'
}
compile('com.google.apis:google-api-services-sheets:v4-rev466-1.22.0') {
    exclude group: 'org.apache.httpcomponents'
    exclude group: 'com.google.code.findbugs'
}
```

### Updating

To update to a new version, open up the module in a file manager (under $PROJECT_DIRECTORY/$MODULE_NAME) and replace the `.aar` with the new one.  Then update the module's `build.gradle` file to point to the new version.

## Authentication Notes

Follow the first two steps of the [Android Quickstart](https://developers.google.com/sheets/api/quickstart/android) for Google Sheets, and make sure the package name in the Google API console matches your app. Debug builds of your app are automatically signed with the key set up in Step 1 of the Quickstart, if you want the release build to work as well you will have to sign it with a different key, as detailed [here](https://developer.android.com/studio/publish/app-signing.html#release-mode), while also changing the information in the Google API console.

## Usage

Create a `CMSC436Sheet` with a `Host` object, and also give it an application name (for the Google Sheets API, see [here](https://bitbucket.org/0queue/google-sheets-helper-library/src/HEAD/googlesheetshelper/src/main/java/cmsc436/tharri16/googlesheetshelper/WriteDataTask.java?at=master&fileviewer=file-view-default#WriteDataTask.java-36)) and a spreadsheet ID, which, in the example, is stored in [strings.xml](https://bitbucket.org/0queue/google-sheets-helper-library/src/HEAD/app/src/main/res/values/strings.xml?at=master&fileviewer=file-view-default).  Then make sure to call the sheet object's `onActivityResult()` and `onRequestPermissionsResult()` methods in their respective Activity methods.  These cut down on boilerplate by automatically handling all the [`Action`s](https://bitbucket.org/0queue/google-sheets-helper-library/src/HEAD/googlesheetshelper/src/main/java/cmsc436/tharri16/googlesheetshelper/CMSC436Sheet.java?at=master&fileviewer=file-view-default#CMSC436Sheet.java-163) the library will do to authorize itself.

The [`Host`](https://bitbucket.org/0queue/google-sheets-helper-library/src/HEAD/googlesheetshelper/src/main/java/cmsc436/tharri16/googlesheetshelper/CMSC436Sheet.java?at=master&fileviewer=file-view-default#CMSC436Sheet.java-154) interface defines callbacks which can be easily be implemented by the host activity.  `getRequestcode()` allows the library to start external activities for the various `Action`s while making sure it doesn't use request codes that are already in use, and because `writeData()` is asynchronous, `notifyFinished()` reports if an exception has occured while writing data, or if writing was successful (in which case `null` is passed in).

For a concise usage example see [MainActivity.java](https://bitbucket.org/0queue/google-sheets-helper-library/src/HEAD/app/src/main/java/cmsc436/tharri16/googlesheetsexample/MainActivity.java?at=master&fileviewer=file-view-default#MainActivity.java).

## Permissions

The library uses the `android.permission.INTERNET`, `android.permission.ACCESS_NETWORK_STATE`, and `android.permission.GET_ACCOUNTS` permissions.  `android.permission.GET_ACCOUNTS` is a runtime permission on API levels 23 and up (Marshmallow).
